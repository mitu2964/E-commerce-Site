<?php require('main/message.php') ?>
<?php require('main/db_connect.php') ?>
<?php
     $query="SELECT * FROM tbl_category";
    $result = mysqli_query($conn,$query);
   
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Ecommerce Site</title>
        <link href="font_end/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="font_end/style.css" rel="stylesheet"/>
        <link href="font_end/css/responsive.css" rel="stylesheet"/>
    </head>
    <body>
        <div class="header_top">
            <div class="left">
                <a href="#"><i class="fa fa-phone">Call +001 555 801</i></a>
            </div>
            <div class="right">
                <div class="main_menu">	
                    <ul id="nav">
                        <li><a href="">
                            <?php
                                if(isset($_SESSION['loggedin'])==true){
 
                                    echo "WELCOME";
                                    echo "&nbsp";
                                    echo $_SESSION['name'];
                            
                                }else{
                                }
                            ?>
                            </a></li>
                        <li><a href="form.php">REGISTRATION FORM</a></li>
                        <li><a href="#">MY WHISTLIST</a></li>
                         <?php
                                if(isset($_SESSION['loggedin'])==true){
 ?>
   <li class="border"><a href="logout.php">LOGOUT</a></li>
                        <?php
                                }else{
                                    ?>
                                    <li class="border"><a href="form_login.php">LOGIN</a></li>
                        <?php
                                }
                            ?>
                        
                    </ul>
                </div>
            </div>
        </div>
        <div class="header_bottom">
            <div class="menu">
                <ul id="nav">			 
                    <li><a href="" class="active">Home</a></li>
                    <li><a href="gallary.php">Gallery</a></li>
                    <li>
                        <a href="#">Categories</a>
                        <ul class="sub_menu">
                            <?php
                                        while($data = mysqli_fetch_array($result)){
                                    ?>
                                    <li>
                            <?php
    echo "<a href='category.php?category_id=$data[category_id]'>{$data['category_name']}</a>";
                                            ?>
                                    </li>
                                    <?php
                                        }
                                            ?>
                        </ul>
                    </li>
                    <li><a href="">Contact</a></li>
                </ul>
            </div>
            <div class="text">
                <h1>Shophia</h1>
            </div>
            <div class="social_icon">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                <a href="#"><i class="fa fa-camera-retro"></i></a>
            </div>
        </div>
        <div class="slider_area">
            <div class="slider-text-table">
                <div class="slider-text-table-cell">
                    <h5>MID-SEASON</h5>
                    <h3>SALE</h3>
                    <img src="font_end/image/upto.png"/>
                    <a class="btn-filled" href="">SHOP NOW</a>
                </div>
            </div>
        </div>
        <div class="shopping_area">
            <div class="shop_text">
                <i class="fa fa-plane"></i>
                <h2>FREE SHIPPING</h2>
                <p>In Order Min $200</p>
            </div>
            <div class="returns_text">
                <i class="fa fa-clock-o"></i>
                <h2>30-DAYS RETURNS</h2>
                <p>Money Back  Guarantee</p>
            </div>
            <div class="support_text">
                <i class="fa fa-futbol-o"></i>
                <h2>24/7 SUPPORT</h2>
                <p>Lifestyme Support</p>
            </div>
        </div>