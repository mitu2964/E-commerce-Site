<?php require('main/message.php') ?>
<body style="background-color: #ddd;">
    <center>
        <div class="login" style="margin-top:250px;">
                    <?php
                        echo message();
                    ?>
                    <form method="POST" action="login.php">
                        <div class="form-group">
                            <label for="exampleInputEmail1">UserName</label>
                            <input type="text"
                                   class="form-control"
                                   name="user_name"
                                   placeholder="UserName">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" 
                                   class="form-control" 
                                   name="user_pass"
                                   placeholder="Password">
                        </div>

                        <button type="submit" name="Login" class="btn btn-default">Login</button>
                    </form>      
                </div>
    </center>
</body>