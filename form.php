<?php require('main/template/font/header.php') ?>
<div class="form_text">
    <h2>REGISTRY FORM</h2>
</div>

<div class="form_area">
    <form role="form" action="create_user.php" name="form" method="POST" enctype="multipart/form-data">

        <div class="form-group">
            <label class="s_name">Name</label>
            <input class="form-control name" name="user_name">
        </div>
        
        <div class="form-group">
            <label class="s_phone">Phone Number</label>
            <input class="form-control phone" name="user_phone">
        </div>
        
        <div class="form-group">
            <label class="s_email">Email</label>
            <input class="form-control mail" name="user_email">
        </div>

        <div class="form-group">
            <label class="s_pass">Password</label>
            <input class="form-control passwo" name="user_pass">
        </div>
        
        <button type="submit" class="btn btn-success submit_button" name="submit">SAVE</button>

    </form>
</div>
<?php require('main/template/font/footer.php') ?>