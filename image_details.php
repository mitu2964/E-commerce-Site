<?php require('main/function.php') ?>
<?php require('main/template/font/header.php') ?>
<?php
$id = $_GET['product_id'];

$query = "SELECT * FROM tbl_product WHERE product_id = $id";

$result = mysqli_query($conn, $query);
confirm($result);
?>

        <div class="summer_collection">
            <div class="summer_text">
                <h2>Summer Collection</h2>
            </div>
            <div class="summer_gallary" style="height:350px">
                <?php
                    while ($row = mysqli_fetch_object($result)) {
                ?>
                <div class="gallary">
                        <img style="width: 400px;height:500px" src="font_end/image/<?php echo $row->product_image; ?>" alt="<?php echo $row->product_image; ?>" class="img-responsive"/>
                        
                </div>
                <div class="gallary_th" style="margin-left:600px">
                        <?php
                        if($row->product_offer){
                        
                        ?>
                        <p><?php echo $row->product_offer; ?></p>
                        
                        <?php
                            }
                        ?>
                        <div class="list_details">
                            <h3 style="margin-bottom: 15px;">Name:<?php echo $row->product_name; ?></h3>
                            <h4>Price:<?php echo $row->product_price; ?></h4>
                            <h4>Code:<?php echo $row->product_code; ?></h4>
                        </div>
                        <div class="img_details" style="margin-top: 100px;">
                            <h1>DETAILS</h1>
                            <?php echo $row->product_details; ?>
                        </div>
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>

<?php require('main/template/font/footer.php') ?>

    