<?php 
    require('../main/function.php') 
?>
<?php 
    require('../main/db_connect.php') 
?>
<?php 
    require('../main/template/header.php') 
?>

<?php
    $current_id = $_GET['product_id'];
    
    $sql ="SELECT * FROM tbl_product WHERE product_id = $current_id";

    $result = mysqli_query($conn, $sql);
    $data = mysqli_fetch_assoc($result);
?>
<?php
     $query="SELECT * FROM tbl_category";
    $result = mysqli_query($conn,$query);
   
?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit Product
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Edit Product
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" action="" enctype="multipart/form-data" method="POST">

                            <div class="form-group">
                                <label>Product Name</label>
                                <input class="form-control" name="product_name" value="<?php echo $data['product_name']?>">
                            </div>

                            <div class="form-group">
                                <label>Product Code</label>
                                <input class="form-control" name="product_code" value="<?php echo $data['product_code']?>">
                            </div>

                            <div class="form-group">
                                <label>Product Price</label>
                                <input class="form-control" name="product_price" value="<?php echo $data['product_price']?>">
                            </div>

                            <div class="form-group">
                                <label>Product Image</label>
                                <input type="file" name="product_image" value="<?php echo $data['product_image']?>">
                            </div>

                            <div class="form-group">
                                <label>Product Details</label>
                                <textarea class="form-control" rows="3" name="product_details"><?php echo $data['product_details']?></textarea>
                            </div>

                            <div class="form-group">
                                <label>Product Offers</label>
                                <input class="form-control" name="product_offer" value="<?php echo $data['product_offer']?>">
                            </div>

                            <div class="form-group">
                                <label>Product Quantity</label>
                                <input class="form-control" name="product_quantity" value="<?php echo $data['product_quantity']?>">
                            </div>

                            <div class="form-group">
                                <label>Product Category</label>
                                <select class="form-control"  name="category_id">
                                    <?php
                                        while($row = mysqli_fetch_array($result)){
                                    ?>
                                    <option value="<?php echo $row["category_id"];?>">
                                        <?php
                                            echo $row["category_name"];
                                            ?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <button name="submit" type="submit" class="btn btn-success">Submit Button</button>

                            </div>
                    </form>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>

<?php
if(isset($_POST['submit'])){
$conn = mysqli_connect("localhost", "root", "", "db_ecommerce");

if (!$conn) {
    die("database failed to connect") . mysqli_connect_error();
}

    move_uploaded_file($_FILES['product_image']['tmp_name'], "images/".$_FILES['product_image']['name']);

    $name = $_POST['product_name'];
    $code = $_POST['product_code'];
    $price = $_POST['product_price'];
    $image = $_FILES['product_image']['name'];
    $details = $_POST['product_details'];
    $offer = $_POST['product_offer'];
    $quantity = $_POST['product_quantity'];

    $query = "UPDATE tbl_product SET product_name='$name', product_code='$code', product_price='$price', product_image='$image', product_details='$details', product_offer='$offer',product_quantity='$quantity' WHERE product_id= $current_id";

    mysqli_query($conn, $query);
}
?>