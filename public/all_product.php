<?php require('../main/message.php') ?>
<?php require('../main/function.php') ?>
<?php
if($_SESSION['loggedin'] && $_SESSION['loggedin']==true){
    
}else{
    redirect_to('index.php');
}
?>
<?php require('../main/db_connect.php') ?>
<?php 
    require('../main/template/header.php') 
?>

<?php
    $query = "SELECT * FROM tbl_product JOIN tbl_category on tbl_category.category_id=tbl_product.category_id";

    $result = mysqli_query($conn, $query);
    confirm($result);
 
?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            All Product
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> All Product
                                <?php 
                                    echo message();
                                ?>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <h2>Bordered Table</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NAME</th>
                                        <th>CODE</th>
                                        <th>PRICE</th>
                                        <th>IMAGE</th>
                                        <th>OFFERS</th>
                                        <th>QUANTITY</th>
                                        <th>CATEGORY</th>
                                        <th>EDIT/DELETE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        ?>                                 
                                <tr>
                                    <td><?php echo $row['product_id']; ?></td>
                                    <td><?php echo $row['product_name']; ?></td>
                                    <td><?php echo $row['product_code']; ?></td>
                                    <td><?php echo $row['product_price']; ?></td>
                                    <td><img style="width: 200px;height:200px" src="../font_end/image/<?php echo $row['product_image']; ?>"
                                             alt="<?php echo $row['product_image']; ?>" class="img-responsive"/></td>
                                    <td><?php echo $row['product_offer']; ?></td>
                                    <td><?php echo $row['product_quantity']; ?></td>
                                    <td><?php echo $row['category_name']; ?></td>
                                    <td>
                                        <a href="edit.php?product_id=<?php echo $row['product_id']; ?>" class="btn btn-success">Edit</a>
                                        <a href="delete.php?product_id=<?php echo $row['product_id']; ?>" class="btn btn-danger">DELETE</a>
                                    </td>
                                </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
<?php
mysqli_free_result($result);
?>
<?php require('../main/template/footer.php') ?>
