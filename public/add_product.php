<?php require('../main/message.php') ?>
<?php require('../main/function.php') ?>
<?php
if($_SESSION['loggedin'] && $_SESSION['loggedin']==true){
    
}else{
    redirect_to('index.php');
}
?>
<?php require('../main/db_connect.php') ?>
<?php
     $query="SELECT * FROM tbl_category";
    $result = mysqli_query($conn,$query);
   
?>
<?php 
    require('../main/template/header.php') 
?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Product
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Add Product
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" action="create_product.php" enctype="multipart/form-data" method="POST">

                            <div class="form-group">
                                <label>Product Name</label>
                                <input class="form-control" name="product_name">
                            </div>

                            <div class="form-group">
                                <label>Product Code</label>
                                <input class="form-control" name="product_code">
                            </div>

                            <div class="form-group">
                                <label>Product Price</label>
                                <input class="form-control" name="product_price">
                            </div>

                            <div class="form-group">
                                <label>Product Image</label>
                                <input type="file" name="product_image">
                            </div>

                            <div class="form-group">
                                <label>Product Details</label>
                                <textarea class="form-control" rows="3" name="product_details"></textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>Product Offer</label>
                                <input class="form-control" name="product_offer">
                            </div>
 
                            <div class="form-group">
                                <label>Product Quantity</label>
                                <input class="form-control" name="product_quantity">
                            </div>
<!--  -->
                            <div class="form-group">
                                <label>Product Category</label>
                                 
                                <select class="form-control"  name="category_id">
                                    <?php
                                        while($data = mysqli_fetch_array($result)){
                                    ?>
                                    <option value="<?php echo $data["category_id"];?>">
                                        <?php
                                            echo $data["category_name"];
                                            ?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                                
                            </div>
  
                            <div class="form-group">
                                <button name="submit" type="submit" class="btn btn-success">Submit Button</button>

                            </div>
                    </form>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

<?php require('../main/template/footer.php') ?>
