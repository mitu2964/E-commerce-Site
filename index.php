<?php require('main/template/font/header.php') ?>
<?php require('main/function.php') ?>
<?php
$query = "SELECT * FROM tbl_product ORDER by product_id DESC LIMIT 8 ";

$result = mysqli_query($conn, $query);

confirm($result);
?>

        <div class="hand_pic_area">
            <div class="hand_text">
                <h2>HandPicked </h2>
            </div>
            
            <div class="collection_area">
                <div class="left_col">
                    <img class="imgshoe" src="font_end/image/shoe.jpg"/>
                    <img class="baceimg" src="font_end/image/bace.jpg"/>
                </div>
                <div class="collection">
                    <img src="font_end/image/Shopia-girl.jpg"/>
                </div>
                <div class="right_col">
                    <img class="imgring" src="font_end/image/ring.jpg"/>
                    <img class="imgparce" src="font_end/image/parce.jpg"/>
                </div>
            </div>
        </div>
        <div class="summer_collection">
            <div class="summer_text">
                <h2>Summer Collection</h2>
            </div>
            <div class="summer_gallary">
                <?php
                    while ($row = mysqli_fetch_object($result)) {
                ?>
                <div class="gallary">
                        <img style="width: 250px;height:250px" src="font_end/image/<?php echo $row->product_image; ?>" alt="<?php echo $row->product_image; ?>" class="img-responsive"/>
                    <div class="th_gallary">
                        <a class="image_details" href="image_details.php?product_id=<?php echo $row->product_id; ?>"><i class="fa fa-picture-o"></i></a>
                        <?php
                        if($row->product_offer){
                        
                        ?>
                        <p><?php echo $row->product_offer; ?></p>
                        <a class="image_details" href="form_login.php"><i class="fa fa-picture-o"></i></a>
                        <?php
                            }
                        ?>
						<a class="btn btn-primary" target="_blank" href="main/cart.php?add=<?php echo  $row->product_id?>">Add to cart</a>
                        <div class="list">
                            <h3>Name:<?php echo $row->product_name; ?></h3>
                            <h4>Price:<?php echo $row->product_price; ?></h4>
                            <h4>Code:<?php echo $row->product_code; ?></h4>
                        </div>
                    </div>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
        <div class="our_brand_area">
            <div class="brand_text">
                <h2>Our Brand</h2>   
            </div>
            <div class="brand_info">
                <img src="font_end/image/bakery.png" alt="" name="slide" height="80px" width="80px"/>
                <img src="font_end/image/caveli.png" alt="" name="slide" height="80px" width="80px"/>
                <img src="font_end/image/harvest.png" alt="" name="slide" height="80px" width="80px"/>
                <img src="font_end/image/land.png" alt="" name="slide" height="80px" width="80px"/>
                <img src="font_end/image/spec.png" alt="" name="slide" height="80px" width="80px"/>
                <img src="font_end/image/home.png" alt="" name="slide" height="80px" width="80px"/>
            </div>
        </div>
        <div class="customer_area">
            <div class="customer_text">
                <h2>Customer Says</h2>
            </div>
            <div class="bg-customer">
                <div class="customer_left">
                    <img src="font_end/image/lady.png"/>
                    <div class="text-customer_left">
                        <p>Sed ut perspiciatis<br> unde omnis iste natus error sit<br> voluptatem accusantium doloremque </p>
                        <h2>Sandra Dewi<span>FASHION STYLISH</span></h2>
                    </div>
                </div>
                <div class="customer_right">
                    <img src="font_end/image/gents.png"/>
                    <div class="text-customer_right">
                        <p>Sed ut perspiciatis<br> unde omnis iste natus error sit<br> voluptatem accusantium doloremque </p>
                        <h2>Shaheer Sheikh<span>DESIGNER</span></h2>
                    </div>
                </div>
            </div>
        </div>
<?php require('main/template/font/footer.php') ?>