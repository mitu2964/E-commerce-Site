
<?php require('main/template/font/header.php') ?>
<div class="form_text">
    <h2>BUY PRODUCT FORM</h2>
</div>


<div class="form_area">
    <form role="form" action="create_buyer.php" name="form" method="POST" enctype="multipart/form-data">

        <div class="form-group">
            <label class="s_name">Name</label>
            <input class="form-control name" name="buyer_name">
        </div>
        
        <div class="form-group">
            <label class="s_phone">Phone Number</label>
            <input class="form-control phone" name="buyer_phone">
        </div>
        
        <div class="form-group">
            <label class="s_email">Address</label>
            <input class="form-control mail" name="buyer_address">
        </div>
        
        <button type="submit" class="btn btn-success submit_button" name="submit">SAVE</button>

    </form>
</div>
<?php require('main/template/font/footer.php') ?>